#include <iostream>
#include <algorithm>
#include <fstream>
#include <math.h>
#include <vector>
#include <queue>
#include <thread>
#include <climits>

using namespace std;

// Describes a job's attributes
struct job {
    int jobID;
    int timeAvail;
    int timeStarted[3] = {-1,-1,-1};
    int timeMachine[3];

    // Determines priority of job on a machine
    double priority[3] = {1.0,1.0,1.0};
};

// Describes a machines's attributes
struct machine {
    job* currentjob;
    bool isFree = true; // machine is always availble at the start
};

class Compare1 {
    public:
    bool operator() (job* a, job* b) {
        return (*a).priority[0] > (*b).priority[0];
    }
};

class Compare2 {
    public:
    bool operator() (job* a, job* b) {
        return (*a).priority[0] > (*b).priority[0];
    }
};

class Compare3 {
    public:
    bool operator() (job* a, job* b) {
        return (*a).priority[0] > (*b).priority[0];
    }
};

bool JobTimeAvailComp(job a, job b);
bool JobIDComp(job a, job b);
pair <int, job*> genOut(job* allJobs, int num_jobs);
void getNeighbor(job* alljobs, int num_jobs);
pair <int, job*> anneal(vector<job> allJobs, int num_jobs);
void solutionCollector(int i, pair <int, job*>* solarr, vector<job> allJobs, int num_jobs);

int main(){
    // Step 1: Getting jobs from file
    int numJobs, numComputers;
    int totalTime = 0;

    cin >> numJobs;
    cin >> numComputers;

    vector<job> allJobs (numJobs);
    machine computers[numComputers];

    // Read in specific jobs
    for (int i = 0; i < numJobs; i++){

        allJobs[i].jobID = i;
        cin >> allJobs[i].timeAvail;

        for (int j = 0; j < numComputers; j++){
            cin >> allJobs[i].timeMachine[j];
        }
    }

    //sort(allJobs.begin, allJobs.begin + numJobs, JobTimeAvailComp);



    // Step 2: Simulated annealing Multithread Code

    const int numThreads = 2;
    thread annealers[numThreads];
    pair <int, job*> solutions [numThreads];
    pair<int, job*> solution;
    solution.first = INT_MAX;

    for (int i = 0; i < numThreads; i++){
        annealers[i] = thread(solutionCollector,i ,solutions, allJobs, numJobs);
    }

    for (int i = 0; i < numThreads; i++){
        annealers[i].join();
    }

    for (int i = 0 ; i < numThreads; i++){
        if (solutions[i].first < solution.first) solution = solutions[i];
    }

    //pair<int, job*> solution = anneal(allJobs, numJobs);
    //anneal(allJobs, numJobs);        
    
    // Step 3: win algobowl
    // Step 4: ???
    // Step 5: Profit    

    // Output
    ofstream output;
    output.open("output.txt");

    output << solution.first << endl;

    job* jobs = solution.second;

    //sort(allJobs.begin, allJobs.begin + numJobs, JobIDComp);
    for (int i = 0; i < numJobs; i++){
        output << jobs[i].timeStarted[0];
        output << ' ' << jobs[i].timeStarted[1];
        output << ' ' << jobs[i].timeStarted[2] << endl;
    }  
    output.close();
    return 0;
}

//Generates an output
pair <int, job*> genOut(job* allJobs, int num_jobs){
    int current_time = 0;
    int jobs_finished = 0;
    machine workstations[3];
    priority_queue<job*, vector<job*>, Compare1> work_queues[3];

    while (jobs_finished < num_jobs) {
        // add jobs to queues if they are now available
        if (current_time <= 50) {
            for (int i = 0; i < num_jobs; i++) {
                if (allJobs[i].timeAvail == current_time) {
                    for (int j = 0; j < 3; j++) {
                        work_queues[j].push(&allJobs[i]);                        
                    }
                }
            }
        }
    
        for (int i = 0; i < 3; i++) {
            // is a job being worked on in workstation, is job done, if yes set isFree to true
            
            if (!workstations[i].isFree && 
                (*workstations[i].currentjob).timeStarted[i] + (*workstations[i].currentjob).timeMachine[i] <= current_time) {

                workstations[i].isFree = true;
                if ((*workstations[i].currentjob).timeStarted[0] != -1 
                && (*workstations[i].currentjob).timeStarted[1] != -1 
                && (*workstations[i].currentjob).timeStarted[2] != -1) {
                    jobs_finished++;
                }
            }
            // check if workstation is available
            if (workstations[i].isFree){
                if (work_queues[i].empty()) {
                    continue;
                }
                // populate workstations with top of queue, unless job in other station
                if ((!workstations[0].isFree && (*work_queues[i].top()).jobID == (*workstations[0].currentjob).jobID) 
                || (!workstations[1].isFree && (*work_queues[i].top()).jobID == (*workstations[1].currentjob).jobID) 
                || (!workstations[2].isFree && (*work_queues[i].top()).jobID == (*workstations[2].currentjob).jobID)){
                    
                    job* tempiBoi = work_queues[i].top();
                    work_queues[i].pop();
                    if (!work_queues[i].empty()) {
                        if ((!workstations[0].isFree && (*work_queues[i].top()).jobID == (*workstations[0].currentjob).jobID) 
                        || (!workstations[1].isFree && (*work_queues[i].top()).jobID == (*workstations[1].currentjob).jobID) 
                        || (!workstations[2].isFree && (*work_queues[i].top()).jobID == (*workstations[2].currentjob).jobID)){
                            
                            job* tempiBoi2 = work_queues[i].top();
                            work_queues[i].pop();
                            if (!work_queues[i].empty()) {
                                workstations[i].currentjob = work_queues[i].top();                                                                                                        
                                work_queues[i].pop();
                                (*workstations[i].currentjob).timeStarted[i] = current_time;
                                workstations[i].isFree = false;
                            } 
                            work_queues[i].push(tempiBoi2);                        
                        }
                        else {
                            workstations[i].currentjob = work_queues[i].top();                                                                                                        
                            work_queues[i].pop();
                            (*workstations[i].currentjob).timeStarted[i] = current_time;   
                            workstations[i].isFree = false;
                        }
                    }
                    work_queues[i].push(tempiBoi);                    
                }
                else {
                    workstations[i].currentjob = work_queues[i].top();                                                                                                        
                    work_queues[i].pop();
                    (*workstations[i].currentjob).timeStarted[i] = current_time;  
                    workstations[i].isFree = false;                
                }
            }
        }
        current_time++;
    }
 
    return make_pair(current_time - 1, allJobs);
}

bool JobTimeAvailComp(job a, job b){
    return a.timeAvail < b.timeAvail;
}

bool JobIDComp(job a, job b){
    return a.jobID < b.jobID;
}

//Create a neighbor job[]
void getNeighbor(job* alljobs, int num_jobs, double percent_jobs) {
    int job;
    for (int i = 0; i < (int)(num_jobs * percent_jobs); i++) {
        job = (rand() % num_jobs);
        alljobs[job].priority[0] *= .5 + (double)rand() / RAND_MAX;
        alljobs[job].priority[1] *= .5 + (double)rand() / RAND_MAX;
        alljobs[job].priority[2] *= .5 + (double)rand() / RAND_MAX;
    }
    return;
}

pair <int, job*> anneal(vector<job> allJobs, int num_jobs) {
    job* arrayAllJobs = &allJobs[0];        
    double temp = 1000;
    double cooling_factor = 0.5;
    pair <int, job*> best_out;
    pair <int, job*> current_out;
    pair <int, job*> new_out;
    current_out = genOut(arrayAllJobs, num_jobs); // copy of
    best_out.first = current_out.first;
    for (int i = 0; i < num_jobs; i++) {
        best_out.second[i] = current_out.second[i];
    }
    while (temp > 999) {
        //delete[] new_out.second;  
        job* p1;
        p1 = new job[num_jobs];
        for (int i = 0; i < num_jobs; i++){
            p1[i] = current_out.second[i];
            p1[i].timeStarted[0] = -1;
            p1[i].timeStarted[1] = -1;
            p1[i].timeStarted[2] = -1;
        }
        getNeighbor(p1, num_jobs, temp/1000);        //pass pointer and have it alter priority values
        //cout << "break" << endl;
        new_out = genOut(p1, num_jobs);
        bool orig_best_del = false;
        if (new_out.first < current_out.first 
        || exp((current_out.first - new_out.first) / temp) > (rand()/(double)RAND_MAX)) {

            //if (current_out.second != best_out.second) current_out.second;

            current_out = new_out;      //copy of
            
            if (current_out.first < best_out.first) {              
                //delete[] best_out.second;
                best_out.first = current_out.first;
                for (int i = 0; i < num_jobs; i++) {
                    best_out.second[i] = current_out.second[i];
                }   
            }
        }
        temp *= (1 - cooling_factor);

        //cout << "Temp: " << temp << endl;
        //cout << current_out.first <<  ":" << best_out.first <<  endl;
        // cout << "Temp: " << temp << endl;
        // cout << current_out.first <<  ":" << best_out.first <<  endl;
    }
    return best_out;
}

void solutionCollector(int i, pair <int, job*>* solarr, vector<job> allJobs, int num_jobs){
    solarr[i] = anneal(allJobs, num_jobs);
}
