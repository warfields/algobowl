import sys

# To use call `python3 input_verifier.py test.txt`

input_name = sys.argv[1]

input_file = open(input_name, "r")
num_jobs, num_stations = list(map(int, input_file.readline().split()))
if num_stations != 3:
    raise Exception("must have three workstations")
if num_jobs > 1000 or num_jobs < 1:
    raise Exception("must have between 1 and 1000 jobs")
for i in range(0, num_jobs + 1):
    if i == num_jobs:
        try:
            avail, t1, t2, t3 = list(map(int, input_file.readline().split()))
            raise Exception("more jobs than stated")
        except ValueError:
            break
        except Exception as e:
            raise e
    else:
        try:
            avail, t1, t2, t3 = list(map(int, input_file.readline().split()))
        except:
            raise Exception("fewer jobs than expected")
        if avail > 50 or avail < 0:
            raise Exception("time available should be between times [0,50]")
        for t in [t1, t2, t3]:
            if t > 50 or t < 1:
                raise Exception("runtime must in range [1,50]")
print("valid input")
