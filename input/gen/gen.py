import random

f = open('input.txt', "w")
f.write("1000 3\n")

# timeavail t1 t2 t3
#  0-50       1-50

for i in range(1000):
    arrival_time = random.randint(0,50)
    times = []
    for j in range(3):
        times.append(random.randint(1,50))

    f.write(str(arrival_time) + ' ' + str(times[0]) + ' ' + str(times[1]) + ' ' + str(times[2]) + '\n')

f.close()