import sys

class job:
    def __init__(self, avail, time1, time2, time3, id):
        # format is [time_available, start1, time1, start2, time2, start3, time3]
        self.id = id
        self.avail = [avail, None, time1, None, time2, None, time3]

    def add_start_times(self, start1, start2, start3):
        if self.avail[0] > min(start1, start2, start3):
            raise Exception("Starts before job is available ", self.id)
        if max(start1 + self.avail[2], start2 + self.avail[4]) - min(start1, start2) < self.avail[2] + self.avail[4]:
            raise Exception("Worksation overlap", self.id)
        elif max(start1 + self.avail[2], start3 + self.avail[6]) - min(start1, start3) < self.avail[2] + self.avail[6]:
            raise Exception("Workstation overlap", self.id)
        elif max(start2 + self.avail[4], start3 + self.avail[6]) - min(start2, start3) < self.avail[4] + self.avail[6]:
            raise Exception("Workstation overlap", self.id)
        else:
            self.avail[1] = start1
            self.avail[3] = start2
            self.avail[5] = start3
        return

    def check_overlap(self, other_job):
        # for s, t, r, w in other_job[1::2], other_job[2::2], self.avail[1::2], self.avail[2::2]:
        #     print("made it")
        #     if max(s + t, r + w) - min(s, r) < t + w:
        #         raise Exception("Jobs overlap", self.id)
        if (max(other_job[1] + other_job[2], self.avail[1] + self.avail[2]) - min(other_job[1], self.avail[1]) < self.avail[2] + other_job[2]):
            raise Exception("Jobs overlap", self.id)
        if (max(other_job[3] + other_job[4], self.avail[3] + self.avail[4]) - min(other_job[3], self.avail[3]) < self.avail[4] + other_job[4]):
            raise Exception("Jobs overlap", self.id)
        if (max(other_job[5] + other_job[6], self.avail[5] + self.avail[6]) - min(other_job[5], self.avail[5]) < self.avail[6] + other_job[6]):
            raise Exception("Jobs overlap", self.id)

    def get_finish_time(self):
        return max(self.avail[1] + self.avail[2], self.avail[3] + self.avail[4], self.avail[5] + self.avail[6])


input_name = sys.argv[1]
solution_name = sys.argv[2]

input_file = open(input_name, "r")
num_jobs, num_stations = list(map(int, input_file.readline().split()))
if num_stations != 3:
    raise Exception("must have three workstations")
jobs = []
for i in range(0, num_jobs):
    avail, t1, t2, t3 = list(map(int, input_file.readline().split()))
    jobs.append(job(avail, t1, t2, t3, i))

solution_file = open(solution_name, "r")
finish_time = list(map(int, solution_file.readline().split()))[0]
for i in range(0, num_jobs):
    s1, s2, s3 = list(map(int, solution_file.readline().split()))
    jobs[i].add_start_times(s1, s2, s3)
    if jobs[i].get_finish_time() > finish_time:
        raise Exception("Incorrect End Time")

for i in range(0, num_jobs):
    for j in range(0, num_jobs):
        if j == i:
            break
        jobs[i].check_overlap(jobs[j].avail)
print("valid")

